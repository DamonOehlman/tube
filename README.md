# Tube

Tube is a proof of concept that explores the prospect of serving JS files (and other resources) over a HTTP text/event-stream connection.  Based on the work done here so far, the concept looks reasonably sound and will be further developed as realtime microframework [fluxer](https://github.com/DamonOehlman/fluxer).

