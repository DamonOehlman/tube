function Manifest() {
    this.resources = [];
}

Manifest.prototype = {
    add: function(data) {
        // add the resource
        this.resources.push(data);
    }
};

Object.defineProperty(Manifest.prototype, 'files', {
    get: function() {
        return this.resources.map(function(resource) {
            return resource.path;
        });
    }
});

if (typeof module != 'undefined' && module.exports) {
    module.exports = Manifest;
}