var assert = require('assert'),
    browser = require('zombie'),
    server = require('./server'),
    page;

describe('tube connection tests', function() {
    before(function(done) {
        server.listen(3000, '0.0.0.0', 5000, done);
    });
    
    after(function(done) {
        server.close(done);
    });
    
    it('should be able to connect to the server', function(done) {
        browser.visit('http://localhost:3000/test/index.html', function(err, instance) {
            assert.ifError(err);
            
            // console.log(instance.window.tube);
            done();
        });
    });
});