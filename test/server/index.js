var tako = require('tako'),
    path = require('path'),
    app = tako(),
    tube = require('../../');

app.route('/test/*').files(path.resolve(__dirname, 'static'));
app.route('/js/*.js').files(path.resolve(__dirname, '..', '..'));
app.route('/tube', tube()).methods('GET');

module.exports = app.httpServer;